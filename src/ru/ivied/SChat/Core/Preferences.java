package ru.ivied.SChat.Core;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.Login;
import ru.ivied.SChat.Sites.FactorySite;

/**
 * Created by Try_harder on 28.06.2014.
 */
public class Preferences {


    private static SharedPreferences getSharedPreferences() {
        return PreferenceManager
            .getDefaultSharedPreferences(MyApp.getContext());
    }

    public static SharedPreferences getSiteSharedPreferences() {
       return MyApp.getContext().getSharedPreferences(Login.XML_LOGIN, 0);
    }

    public static int getAmountOfVisibleRows() {
        return Integer.parseInt(getSharedPreferences().getString("count", "30"));
    }

    public static boolean isMessageStringShow() {
        return getSharedPreferences().getBoolean("stringMessage", true);
    }

    public static boolean isMessageLinksShow() {
        return getSharedPreferences().getBoolean("linkShow", true);
    }

    public static boolean isAutoScrollChat() {
        return getSharedPreferences().getBoolean("autoScroll", true);
    }

    public static boolean isMessageDelete() {
        return getSharedPreferences().getBoolean("deleteMessage", true);
    }

    public static boolean isShowSmiles() {
        return getSharedPreferences().getBoolean("showSmiles", true);
    }

    public static boolean isUpdateSmiles() {
        return getSharedPreferences().getBoolean("updateSmiles", false);
    }

    public static boolean isShowChannelsInfo() {
        return getSharedPreferences().getBoolean("showChannelInfo", true);
    }

    public static boolean isShowSiteLogo() {
        return getSharedPreferences().getBoolean("showSiteLogo", true);
    }

    public static boolean isShowNotifySystem() {
        return getSharedPreferences().getBoolean("notifySystem", true);
    }

    public static int getHeightOfVideo() {
        return Integer.parseInt(getSharedPreferences().getString("videoPixels", "250"));
    }

    public static boolean isShowNotifyHeaders() {
        return getSharedPreferences().getBoolean("notifyHeaders", true);
    }


    public static Credentials getCredentialsForSite (FactorySite.SiteName siteName){
        return new Credentials(getSiteSharedPreferences().getString(siteName.name(), ""), getSiteSharedPreferences().getString(siteName.name() + "pass", ""));
    }

    public static Credentials getCredentialsForSite (String siteName){
        return new Credentials(getSiteSharedPreferences().getString(siteName, ""), getSiteSharedPreferences().getString(siteName + "pass", ""));
    }

}
