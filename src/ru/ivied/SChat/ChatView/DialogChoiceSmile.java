package ru.ivied.SChat.ChatView;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import ru.ivied.SChat.Core.MainActivity;
import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;
import ru.ivied.SChat.Sites.SmileHandler;
import ru.ivied.SChat.Sites.SmileHandler_;

public class DialogChoiceSmile extends DialogFragment implements  OnClickListener {
    DialogSmilesBySite dialogSmilesShow;
    FactorySite mFactorySite =new FactorySite();
    public static final int TEXT_VIEW_TOP_MARGINS = 20;
    public static final int REFRESH_BUTTON_ID = 10;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getResources().getString(R.string.dialog_title_select_smile_site));
        LinearLayout layout = new LinearLayout(getActivity());
        Drawable drawable = mFactorySite.getSite(FactorySite.SiteName.SC2TV).getLogo();
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT));
        TextView downloadButtonText = new TextView(getActivity());
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        Drawable refreshLogo = getResources().getDrawable(R.drawable.smile_refresh_button);
        Bitmap bitmap = ((BitmapDrawable) refreshLogo).getBitmap();
        refreshLogo = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth(), true));
        downloadButtonText.setCompoundDrawablesWithIntrinsicBounds(refreshLogo, null, null, null);
        llp.setMargins(0, TEXT_VIEW_TOP_MARGINS, 0, 0);

        downloadButtonText.setText(getString(R.string.btn_refresh_smiles));
        downloadButtonText.setGravity(Gravity.CENTER);
        downloadButtonText.setLayoutParams(llp);
        downloadButtonText.setId(REFRESH_BUTTON_ID);
        downloadButtonText.setOnClickListener(this);
        layout.addView(downloadButtonText);
        for(FactorySite.SiteName siteName : FactorySite.SiteName.values()){
            TextView textSite = new TextView(getActivity());
            String text = "   " +  siteName.name() + " smiles";
            drawable =  mFactorySite.getSite(siteName).getLogo();
            textSite.setText(text);
            textSite.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            textSite.setGravity(Gravity.CENTER);
            textSite.setLayoutParams(llp);
            textSite.setId(MainActivity.ID_SITE_SELECT + siteName.ordinal());
            textSite.setOnClickListener(this);
            layout.addView(textSite);

        }
        
        return layout;
    }


	@Override
	public void onClick(View v) {
		onDismiss(getDialog());


        switch (v.getId()){
            case (REFRESH_BUTTON_ID):
                if (SmileHandler_.getInstance_(MyApp.getContext()).isDownloadActive())
                    Toast.makeText(MyApp.getContext(), R.string.toast_smiles_update_in_progress,
                            Toast.LENGTH_LONG).show();
                else {
                    MyApp.SmileCache.Refresh();
                    Toast.makeText(MyApp.getContext(), R.string.toast_smiles_update_begin,
                            Toast.LENGTH_LONG).show();
                }
                return;
        }

        int id = v.getId() - MainActivity.ID_SITE_SELECT;

        for(FactorySite.SiteName siteName : FactorySite.SiteName.values()){
            if (siteName.ordinal() == id){
                dialogSmilesShow = DialogSmilesBySite.newInstance( siteName);
                dialogSmilesShow.show(getFragmentManager(), "Select smile");
            }
        }
	}

}