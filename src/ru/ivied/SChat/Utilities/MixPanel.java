package ru.ivied.SChat.Utilities;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONObject;

/**
 * Created by Serv on 10.05.2014.
 */
public class MixPanel  {
    public static final String MIXPANEL_TOKEN = "dc1ad0eefaeff4b00c3a3a372efdf8b3";
    private static MixpanelAPI mixPanel ;

    public static void init(Context applicationContext) {
        mixPanel  = MixpanelAPI.getInstance(applicationContext, MixPanel.MIXPANEL_TOKEN);
    }

    public static void track(String eventName, JSONObject props) {
        mixPanel.track(eventName, props);
    }

    public static void flush() {
        mixPanel.flush();
    }
}
