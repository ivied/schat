package ru.ivied.SChat.Sites.Twitch;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.util.Log;

import org.apache.http.client.methods.HttpGet;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.ivied.SChat.AddChat.FragmentAddChannelStandard;
import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.Core.SendMessageService;
import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.FragmentLoginStandard;
import ru.ivied.SChat.Login.LoginException;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;
import ru.ivied.SChat.Sites.Message;
import ru.ivied.SChat.Sites.Site;
import ru.ivied.SChat.Sites.Smile;
import ru.ivied.SChat.Sites.SmileHandler;
import ru.ivied.SChat.Sites.SmileHandler_;

/**
 * Created by Serv on 30.05.13.
 */
public class Twitch  extends Site {

    private static final String TAG = "TwitchSChat";
    final String TWITCH_SMILE_ADDRESS = "https://api.twitch.tv/kraken/chat/emoticons";
    public static IrcClient botSend;
    public static String twitchNick;
    public static String twitchPass;

    ExecutorService es;
    IrcClientShow bot;
    private static Map<String, String> unrecognizedSmiles = new HashMap<String, String>() ;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("\\&lt\\;3", "<3");
        aMap.put("\\&gt\\;\\(", ">\\(");
        unrecognizedSmiles = Collections.unmodifiableMap(aMap);
    }

    @Override
    public Drawable getLogo() {
        return MyApp.getContext().getResources().getDrawable(R.drawable.twitch);
    }
    @Override
    public void readChannel(String channel) {
            int rnd=(int) (1 + Math.random() * 100);
            String random ="justinfan"+Integer.toString(rnd);
            bot = new IrcClientShow(random);
            try {
                bot.connect("199.9.250.229", 6667, "");
            } catch (IOException | IrcException e) {
                Log.w("cant connect to Twitch",  e);
            }

            bot.joinChannel("#" + channel);
    }

    @Override
    public void startThread(Runnable channelRun) {

        es = Executors.newSingleThreadExecutor();
        mFuture = es.submit(channelRun);

    }


    @Override
    public void destroyLoadMessages () {
        bot.disconnect();
        mFuture.cancel(true);
    }

    @Override
    public int sendMessage(String channel, String message) {
        if (twitchNick == null){
            return SendMessageService.NEED_LOGIN;
        }else{
            try {
                botSend.reconnect();
            } catch (IOException | IrcException e) {
                Log.w("cant connect to Twitch", e);
            }
            botSend.sendMessage("#" + channel, message);
            return 0;
        }
    }

    @Override
    public FactorySite.SiteName getSiteEnum() {
        return FactorySite.SiteName.TWITCH;
    }

    @Override
    protected boolean isPrivateMessage(String message) {
        return false;
    }

    @Override
    public Spannable getSmiledText(String text, String nick) {
        int length = nick.length() + 1;
        Spannable spannable = getLinkedSpan( nick , text);
        addSmiles( spannable, length, "");
        return spannable;
    }

    @Override
    public ConcurrentHashMap<String, Smile> getSmileMap() {
        return SmileHandler_.getInstance_(MyApp.getContext()).getTwitchMap();
    }


    public class IrcClientShow extends PircBot {
        public IrcClientShow(String name) {
            this.setName(name);
            //setVerbose(true);
        }

        public void onMessage(String channel, String sender, String login,
                              String hostname, String text) {
            long unixTime = System.currentTimeMillis() / 1000L;
            channel = channel.substring(1);
            Message message = new Message(channel, sender, text, null, unixTime);
            insertMessage ( message );

        }
    }


    @Override
    public FragmentLoginStandard getFragment() {
        return new FragmentLoginTwitch();
    }

    @Override
    public FragmentAddChannelStandard getFragmentAddChannel() {
        return new FragmentFindChannelTwitch();
    }

    @Override
    public int getColorForAdd(String channel, int length, int color) {

        if ((length>3)){
            try {
                String text = stringToHex (channel).substring(0, 6);
                Log.d(TAG, "color = "+ text);
                return Color.parseColor("#" + text);
            } catch (UnsupportedEncodingException e) {
                Log.w("Unsupported encoding", e);
            }
        }
        return color;
    }

    private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
    public String stringToHex(String input) throws UnsupportedEncodingException
    {
        if (input == null) throw new NullPointerException();
        return asHex(input.getBytes());
    }



    private String asHex(byte[] buf)
    {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i)
        {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }

    @Override
    public void getLogin(Credentials credentials) throws LoginException{
        twitchNick = credentials.getNick();
        twitchPass = credentials.getPass();
        Twitch.botSend = new IrcClient(twitchNick);

        try {
            botSend.connect("199.9.250.229", 6667, twitchPass);
        } catch (IOException | IrcException e) {
            Log.w("cant connect to Twitch", e);
        }
    }


    @Override
    public String getSmileAddress() {
        return TWITCH_SMILE_ADDRESS;
    }

    @Override
    public void getSiteSmiles() {
        JSONObject smile ;
        JSONObject image ;
        JSONArray imageList;
        HttpGet httpGet = new HttpGet(getSmileAddress());
        StringBuilder builderJson = jsonRequest(httpGet);
        JSONArray jsonArray ;
        try {
            JSONObject jsonObj = new JSONObject(builderJson.toString());

            jsonArray = jsonObj.getJSONArray("emoticons");

            for(int i = 0 ; i < jsonArray.length(); i++ ){
                smile = jsonArray.getJSONObject(i);
                imageList = smile.getJSONArray("images");
                image = imageList.getJSONObject(0);
                if(image.getString("emoticon_set").equalsIgnoreCase("null")){

                    String regex = smile.getString("regex");
                    String imageAddress = image.getString("url");
                    String width = image.getString("width");
                    String height = image.getString("height");
                    if (unrecognizedSmiles.containsKey(regex))
                        regex = unrecognizedSmiles.get(regex);
                        PutSmile putSmile = new PutSmile(imageAddress, regex, width, height);
                        putSmile.run();
                        Log.i(TAG, "Add new Twitch smile: regex = " + regex + ", Place in JSON = " + i + "  OF  " + jsonArray.length());
                }
           }
        } catch (JSONException e) {
            Log.d(TAG, "Exception", e);
        }
    }


    @Override
    public int getMiniLogo() {
        return R.drawable.twitch_small;
    }

}
