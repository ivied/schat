package ru.ivied.SChat.Login;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import ru.ivied.SChat.Core.MainActivity;
import ru.ivied.SChat.Sites.FactorySite;



public class Login extends SherlockFragmentActivity {
    static final public String XML_LOGIN = "Login";
    protected static final String PASSWORD = "pass";
    DialogSelectLogin mDialogSelectLogin;
    FactorySite factorySite = new FactorySite();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);

        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT));

       for (FactorySite.SiteName siteName : FactorySite.SiteName.values()){

            if (siteName == FactorySite.SiteName.TWITCH) continue;

            LinearLayout innerLayout1 = new LinearLayout(this);

            innerLayout1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            innerLayout1.setId(MainActivity.ID_LOGIN_FRAGMENTS+siteName.ordinal());
            {
                FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();

                fTrans.add(MainActivity.ID_LOGIN_FRAGMENTS+siteName.ordinal(), factorySite.getSite(siteName).getFragment());
                fTrans.commit();
            }
            layout.addView(innerLayout1);
            setContentView(layout);
       }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, 1, 0, "Delete login");


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDialogSelectLogin = new DialogSelectLogin();
        mDialogSelectLogin.show(getSupportFragmentManager(), "Load chat");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            setResult(RESULT_OK, null);

            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }




}
