package ru.ivied.SChat.Sites.Sc2tv;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;


public class Sc2tvTokenRequest extends StringRequest {

    private final TokenListener tokenListener;

    interface TokenListener{
        void setToken(String token);
    }

    public Sc2tvTokenRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, TokenListener tokenListener) {
        super(method, url, listener, errorListener);
        this.tokenListener = tokenListener;
    }


    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
       Map<String, String> responseHeaders = response.headers;
       tokenListener.setToken(responseHeaders.get(""));
       return super.parseNetworkResponse(response);
    }
}
