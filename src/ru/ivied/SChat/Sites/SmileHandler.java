package ru.ivied.SChat.Sites;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.Core.MyContentProvider;
import ru.ivied.SChat.Utilities.GraphicUtilities;

/**
 * Created by Try_harder on 29.05.2014.
 */


@EBean(scope = EBean.Scope.Singleton)
public class SmileHandler {

    private static final String TAG = "SmileSingletonSChat";

    private ConcurrentHashMap<String, Smile> smileMapSC2TV = new ConcurrentHashMap<String, Smile>();
    private ConcurrentHashMap<String, Smile> smileMapTWITCH = new ConcurrentHashMap<String, Smile>();
    private Cursor c;
    private static AtomicInteger threadCount = new AtomicInteger();
    private final static String SC2TV_SITE_NAME = "sc2tv";
    private final static String TWITCH_SITE_NAME = "Twitch";
    private static AtomicBoolean smileDownloadActive = new AtomicBoolean(false);
    final ContentResolver contentResolver = MyApp.getContext().getContentResolver();

    public ConcurrentHashMap<String, Smile> getSc2tvMap() {

        return smileMapSC2TV;

    }

    public ConcurrentHashMap<String, Smile> getTwitchMap() {

        return smileMapTWITCH;

    }

    @AfterInject
    public void onInit() {

        HandlerThread thread = new HandlerThread("observeThread");
        thread.start();
        c = getNewCursor();
        fillMap(c);
        contentResolver.registerContentObserver(MyContentProvider.SMILE_INSERT_URI, true, new ContentObserver(new Handler(thread.getLooper())) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                Thread myThread = new Thread(new Runnable(){
                    @Override
                    public void run()
                    {

                        int currentThreadNumber = threadCount.incrementAndGet();

                        try {
                            synchronized (this) {
                                wait(250);
                            }
                        } catch (InterruptedException e) {
                            Log.wtf(TAG, e);
                        }
                        if(currentThreadNumber != threadCount.get()) {
                            smileDownloadActive.set(true);
                            return;
                        }

                        smileDownloadActive.set(false);
                        Log.v(TAG, "thread  not interrupted ");
                        fillMap(c);
                    }
                });

                myThread.start();

            }
        });

    }

    private Cursor getNewCursor() {
        final String selection = "_id > ?";
        final String[] selectionArgs = { "0" };

        return contentResolver.query(MyContentProvider.SMILE_INSERT_URI, null, selection, selectionArgs, null);
    }


    public synchronized void fillMap(Cursor c) {
        Log.d(TAG, "Map Update begin");
        smileMapSC2TV.clear();
        smileMapTWITCH.clear();
        Cursor cursor = getNewCursor();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String siteName = cursor.getString(1);
            String regexp = cursor.getString(3);
            Smile smile = new Smile(siteName, GraphicUtilities.getImageFromBLOB(cursor.getBlob(2), 1), regexp, cursor.getInt(4), cursor.getInt(5));
            if (siteName.equalsIgnoreCase(SC2TV_SITE_NAME))
                smileMapSC2TV.put(regexp, smile);
            if (siteName.equalsIgnoreCase(TWITCH_SITE_NAME))
                smileMapTWITCH.put(regexp, smile);
        }
        Log.d(TAG, "Map Update end");
        cursor.close();

    }

    public boolean isDownloadActive() {
        return smileDownloadActive.get();
    }

}
