package ru.ivied.SChat.Sites.Sc2tv;

import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.ivied.SChat.AddChat.FragmentAddChannelStandard;
import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.Core.SendMessageService;
import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.FragmentLoginStandard;
import ru.ivied.SChat.Login.LoginException;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;
import ru.ivied.SChat.Sites.Message;
import ru.ivied.SChat.Sites.Site;
import ru.ivied.SChat.Sites.Smile;
import ru.ivied.SChat.Sites.SmileHandler;
import ru.ivied.SChat.Sites.SmileHandler_;


/**
 * Cddreated by Serv on 29.05.13.
 */
public class Sc2tv extends Site {
    final public static Pattern BOLD_PATTERN = Pattern.compile("(\\[b\\])(.*)(\\[\\/b\\])");
    private static final String SC2TV_SMILES = "http://chat.sc2tv.ru/js/smiles.js";
    private static final String TAG = "Sc2tvSChat";
    public static String sc2tvNick;
    public static String sc2tvPass;
    public static String token=null;
    static public HttpClient client = new DefaultHttpClient();
    private static final String CHANNEL_MESSAGES = "http://chat.sc2tv.ru/memfs/channel-";
    private static final String GET_TOKEN = "http://chat.sc2tv.ru/gate.php?task=GetUserInfo&ref=http://sc2tv.ru/";
    private static final String SC2TV_GATE =  "http://chat.sc2tv.ru/gate.php";
    private static final String SC2TV_STANDARD_SMILE_WAY = "http://chat.sc2tv.ru/img/";
    private static final String SC2TV_SMILE_FIELD_ADDRESS = "img";
    private static final String SC2TV_SMILE_FIELD_REGEXP = "code";
    private static final String SC2TV_SMILE_FIELD_WIDTH = "width";
    private static final String SC2TV_SMILE_FIELD_HEIGHT = "height";
    final static StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
    ScheduledExecutorService sEs;


    @Override
    public Drawable getLogo() {
        return MyApp.getContext().getResources().getDrawable(R.drawable.sc2tv);
    }
    @Override
    public void readChannel(String channel) {
        HttpGet httpGet = new HttpGet(CHANNEL_MESSAGES + channel + ".json");
        StringBuilder builderJson = jsonRequest(httpGet);

        JSONArray jsonArray = new JSONArray();
        try {
            if(builderJson.toString().equals("")) return;
            JSONObject jsonObj = new JSONObject(builderJson.toString());


            jsonArray = jsonObj.getJSONArray("messages");
        } catch (JSONException e) {
            Log.d(TAG, "Exception",  e);
        }
        insertSc2tv (jsonArray, 0, channel);

    }

    @Override
    public void startThread(Runnable channelRun) {

        sEs = Executors.newScheduledThreadPool(1);

        mFuture = sEs.scheduleWithFixedDelay(channelRun, 0, 3,
                TimeUnit.SECONDS);
    }

    @Override
    public FragmentLoginStandard getFragment() {
        return new FragmentLoginSC2TV();
    }

    @Override
    public FragmentAddChannelStandard getFragmentAddChannel() {
        return new FragmentFindChannelSc2tv();
    }

    @Override
    public int getColorForAdd(String channel, int length, int color) {
        if (!(length==0)){

            if (length>8) length=8;
            color = -Integer.parseInt(channel.substring(0, length));
        }
        return color;
    }

    @Override
    public void getLogin(Credentials credentials) throws LoginException{

        sc2tvNick = credentials.getNick();
        sc2tvPass = credentials.getPass();
        try {
            client = new DefaultHttpClient();
            HttpPost post = getSc2tvPost(sc2tvNick , sc2tvPass);
            client.execute(post);
            HttpGet httpGet = new HttpGet(GET_TOKEN );

            HttpResponse response = client.execute(httpGet);
            InputStream content = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(   new InputStreamReader(content));
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            JSONObject jsonObj = new JSONObject(builder.toString());

            token = jsonObj.getString("token");
            content.close();

        } catch (JSONException e) {
            throw new LoginException();
        }

        catch (IOException e) {
            Log.d(TAG, "Exception",  e);
        }
    }

    @Override
    public String getSmileAddress() {
        return SC2TV_SMILES;
    }

    @Override
    public void getSiteSmiles() {


        HttpGet httpGet = new HttpGet(getSmileAddress());
        String response = jsonRequest(httpGet).toString();
        try {
            String responseInJson =  response.substring(response.indexOf("[{"), response.indexOf("}]") + 2) + "}";
            JSONArray smileArray = new JSONArray(responseInJson);
            for (int i = 0; i < smileArray.length(); i++) {
                JSONObject smile = smileArray.getJSONObject(i);
                String smileAddress = smile.getString(SC2TV_SMILE_FIELD_ADDRESS);
                PutSmile putSmile = new PutSmile(SC2TV_STANDARD_SMILE_WAY + smileAddress, smile.getString(SC2TV_SMILE_FIELD_REGEXP),
                        smile.getString(SC2TV_SMILE_FIELD_WIDTH),
                        smile.getString(SC2TV_SMILE_FIELD_HEIGHT));
                putSmile.run();

            }
        } catch (JSONException e) {
            Log.e(TAG, "Wrong server Response format", e);
        }
    }

    @Override
    public int getMiniLogo() {
        return R.drawable.sc2tv_small;
    }



    private void insertSc2tv(JSONArray jsonArray, int i, String channel) {
        try {
            if (jsonArray.length() == i) return;
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String[] selectionArgs = new String[] { getSiteName() + jsonObject.getString("id") };
            Cursor c = MyApp.getContext().getContentResolver().query(
                    INSERT_URI, null,
                    "identificator = ?", selectionArgs, null);
            if (c.getCount() == 0) {

                i++;
                c.close();
                insertSc2tv(jsonArray, i, channel);
                i--;
                String nick = jsonObject.getString("name");
                String message = jsonObject.getString("message");
                String id = jsonObject.getString("id");
                long time = 0;
                try {

                    final SimpleDateFormat df = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");
                    final Date d = df.parse(jsonObject.getString("date"));

                    time = d.getTime() / 1000;

                } catch (ParseException e) {
                    Log.d(TAG, "Exception",  e);
                }
                Message messageForInsert = new Message( channel, nick, message, id, time);
                insertMessage( messageForInsert );
                privateMessage( messageForInsert );
            }else {
                c.close();
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception",  e);
        }
    }

    @Override
    protected boolean isPrivateMessage(String message) {

        Matcher matcher = BOLD_PATTERN.matcher(message);
        if (matcher.find()) {

            String address = matcher.group(2);
            if (address.equalsIgnoreCase(sc2tvNick)) return true;
        }
        return false;
    }

    @Override
    public FactorySite.SiteName getSiteEnum() {
        return FactorySite.SiteName.SC2TV;
    }

    @Override
    public Spannable getSmiledText(String text, String nick) {

        int addressLength = 0;
        boolean privateM = false;
        Matcher matcher = BOLD_PATTERN.matcher(text);

        if (matcher.find()) {
            text = text.replace("[b]", "").replace("[/b]", "");

            String address = matcher.group(2);
            privateM = address.equalsIgnoreCase(sc2tvNick);
            addressLength = matcher.group(2).length();
        }
        int length = nick.length() + 1;

        Spannable spannable = getLinkedSpan(nick, text);
        if (addressLength > 0){
            spannable.setSpan(bss, length+1, length + 1 + addressLength, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            if (privateM) {	spannable.setSpan(new ForegroundColorSpan(MyApp.getContext().getResources()
                    .getColor(R.color.private_msg)), length,
                    spannable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        addSmiles( spannable, length, "\\:s");
        return spannable;
    }



    @Override
    public ConcurrentHashMap<String, Smile> getSmileMap() {
        return SmileHandler_.getInstance_(MyApp.getContext()).getSc2tvMap();
    }

    @Override
    public int sendMessage(String channel, String message) {
        if (sc2tvNick == null)
            return SendMessageService.NEED_LOGIN;
        else {

            int count = 0;
            for (ConcurrentHashMap.Entry<String, Smile> entry : SmileHandler_.getInstance_(MyApp.getContext()).getSc2tvMap().entrySet()) {

                Matcher matcher = Pattern.compile(entry.getKey()).matcher(message);
                while (matcher.find()) {
                    count++;
                }
            }
            if (count < 3) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
                        4);

                nameValuePairs.clear();
                nameValuePairs.add(new BasicNameValuePair("task",
                        "WriteMessage"));
                nameValuePairs.add(new BasicNameValuePair(
                        "message", message));
                nameValuePairs.add(new BasicNameValuePair(
                        "channel_id", channel));
                nameValuePairs.add(new BasicNameValuePair("token",
                        Sc2tv.token));
                HttpPost post2 = new HttpPost( SC2TV_GATE  );

                try {
                       post2.setEntity(new UrlEncodedFormEntity(
                       nameValuePairs, HTTP.UTF_8));
                   client.execute(post2);
                } catch (IOException e) {
                    Log.d(TAG, "Exception",  e);
                }

                return SendMessageService.MESSAGE_DELIVER_OK;
            }else{
                return SendMessageService.TOO_MUCH_SMILES_SC2TV;
            }
        }

    }

    public HttpPost getSc2tvPost(String name, String pass) {
        HttpPost post = new HttpPost("http://sc2tv.ru/");
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
                4);
        nameValuePairs.add(new BasicNameValuePair("name", name));
        nameValuePairs.add(new BasicNameValuePair("pass", pass));
        nameValuePairs.add(new BasicNameValuePair("op", "Вход"));
        nameValuePairs.add(new BasicNameValuePair("form_id","user_login_block"));
        try {
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "Exception",  e);
        }
        return post;
    }

}
