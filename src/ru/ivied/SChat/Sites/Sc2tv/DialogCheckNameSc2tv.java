package ru.ivied.SChat.Sites.Sc2tv;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.R;

public class DialogCheckNameSc2tv extends DialogFragment implements OnClickListener {

    private static final String TAG = "DialogCheckNameSc2tvSChat";
    EditText channelName;
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		 channelName = new EditText(getActivity());
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity()).setView(channelName)
				.setTitle(getResources().getString(R.string.dialog_title_check_sc2tv_name))
				.setPositiveButton(android.R.string.ok, this);

		return adb.create();
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {

        EditText channel = (EditText) getActivity().findViewById(R.id.editChannelNumberSc2tv);
        String newChannel = channelName.getText().toString();
        if (!newChannel.equals("")){
            GetSc2tvCode newChannelCode = new GetSc2tvCode();
            newChannelCode.execute(newChannel);

            try {
                newChannel = newChannelCode.get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e(TAG, "Cant get Sc2tv code", e);
            }
        }
        channel.setText(newChannel);
	}

    static class GetSc2tvCode extends AsyncTask<String, Void, String> {


        private static final String SC2_CONTENT = "http://sc2tv.ru/content/";
        private static final String SC2_LOGIN_PATTERN = ".*\\bhttp://sc2tv.ru/node/[0-9]{5}\\b.*";
        @Override
        protected String doInBackground(String... a) {
            StringBuilder result = new StringBuilder();
            for (String subString : a) {
                result.append(subString);
            }
            String channel = result.toString();
            String getCode = MyApp.getContext().getResources().getString(R.string.dialog_answer_wrong_sc2tv_name);
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(SC2_CONTENT  + channel);
            try {

                HttpResponse response = client.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                    BufferedReader reader = new BufferedReader( new InputStreamReader(response.getEntity().getContent()));
                    String line;

                    boolean b = false;
                    while (!b ) {
                        line = reader.readLine();
                        Pattern p = Pattern
                                .compile(SC2_LOGIN_PATTERN );
                        Matcher m = p.matcher(line);
                        b = m.matches();
                        if (b) {
                            getCode = m.group().replaceAll("\\D+", "")
                                    .replaceFirst("2", "");
                        }
                    }
                    reader.close();
                } else {
                    Log.e(TAG, "Failed to download file");
                }
            } catch (IOException e) {
                Log.e(TAG, "Cant get Sc2tv code", e);
            }
            client.getConnectionManager().shutdown();
            return getCode;
        }

    }
}
