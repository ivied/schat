package ru.ivied.SChat.Sites.Twitch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;

import ru.ivied.SChat.AddChat.ChannelIdSelectedListener;
import ru.ivied.SChat.R;

public class DialogFindTwitchChannel extends DialogFragment implements
		OnClickListener {
    private static final String TAG = "DialogFindTwitchChannelSChat";
    EditText input;
	CheckTwitchChannel checkTwitch;
    ChannelIdSelectedListener mCallback;
    final static String CHECK_CHANNEL = "http://api.justin.tv/api/stream/list.json?channel=";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (ChannelIdSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ChannelIdSelectedListener ");
        }
    }

	public Dialog onCreateDialog(Bundle savedInstanceState) {

		input = new EditText(getActivity());
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
				.setTitle(getResources().getString(R.string.dialog_title_check_twitch_channel)).setView(input)
				.setPositiveButton("Add", this);

		return adb.create();

	}


	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		String channel;
		channel = input.getText().toString().trim().split("\\s+")[0];

		if (!channel.equalsIgnoreCase("")) {
			if (!channel.equals("")) {
				checkTwitch = new CheckTwitchChannel();
				checkTwitch.execute(channel);
				try {
					channel = checkTwitch.get();
				} catch (InterruptedException | ExecutionException e) {
                    Log.d(TAG, "Exception", e);
				}
                mCallback.pasteIdChannel(channel);
			}
		}
	}
	
	class CheckTwitchChannel extends AsyncTask<String, Void , String> {


		@Override
		protected String doInBackground(String... a) {
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(CHECK_CHANNEL+ a[0]);
			String line = null;
			try {
				HttpResponse response = client.execute(httpGet);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				line = reader.readLine();
                reader.close();
			} catch (IOException e) {
                Log.d(TAG, "Exception",  e);
			}
            if (line != null && line.length() < 10)	a[0] = "not exist now";
			client.getConnectionManager().shutdown();
			return a[0];
		}
		
		  @Override
		    protected void onPostExecute(String result) {
		      super.onPostExecute(result);
		      
		    }
	}
}
