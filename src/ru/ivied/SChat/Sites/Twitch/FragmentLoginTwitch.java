package ru.ivied.SChat.Sites.Twitch;

import android.graphics.drawable.Drawable;
import android.util.Log;

import org.jibble.pircbot.IrcException;

import java.io.IOException;

import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.FragmentLoginStandard;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;

/**
 * Created by Serv on 01.06.13.
 */
public class FragmentLoginTwitch extends FragmentLoginStandard {
    @Override
    public Drawable getLogo() {
        return getResources().getDrawable(R.drawable.twitch );
    }

    @Override
    public String getFragmentName() {
        return FactorySite.SiteName.TWITCH.name();
    }

    @Override
    public String getTextOnAddBtn() {
        return getResources().getString(R.string.btn_login_twitch);
    }

    @Override
    public boolean tryLogin(Credentials credentials) {
        IrcClient bot = new IrcClient(credentials.getNick());
        try {
            bot.connect("199.9.250.229", 6667, credentials.getPass());
        } catch (IOException | IrcException e) {
            Log.w("Cant connect to Twitch", e);
        }
        if (bot.isConnected()) {
            bot.disconnect();
            return true;

        } else {

            return false;
        }

    }




}
