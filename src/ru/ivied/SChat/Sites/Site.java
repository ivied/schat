package ru.ivied.SChat.Sites;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.ivied.SChat.AddChat.FragmentAddChannelStandard;
import ru.ivied.SChat.ChatView.ActionProviderLink;
import ru.ivied.SChat.ChatView.AdapterChatCursor;
import ru.ivied.SChat.Core.ChatService;
import ru.ivied.SChat.Core.MainActivity;
import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.Core.MyContentProvider;
import ru.ivied.SChat.Core.Preferences;
import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.FragmentLoginStandard;
import ru.ivied.SChat.Login.Login;
import ru.ivied.SChat.Login.LoginException;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Utilities.GraphicUtilities;

/**
 * Created by Serv on 29.05.13.
 */
public abstract class Site {


    private static final String TAG = "SiteSChat";
    boolean bound =false;
    public Future mFuture;
    public static final Uri INSERT_URI = Uri.parse("content://ru.ivied.SChat/chats/insert");
    final Uri ADD_URI = Uri.parse("content://ru.ivied.SChat/channels/add");

    abstract public Drawable getLogo();
    abstract public FragmentLoginStandard getFragment ();
    abstract public FragmentAddChannelStandard getFragmentAddChannel();
    abstract public int getColorForAdd (String channel, int length , int color);
    abstract public  Spannable getSmiledText(String text, String nick);
    abstract public int sendMessage(String channel, String message);
    abstract public ConcurrentHashMap<String, Smile> getSmileMap();
    abstract public void getSiteSmiles();
    abstract public int getMiniLogo();


    abstract protected void readChannel(String channel);
    abstract protected void startThread (Runnable channelRun);
    abstract protected String getSmileAddress();
    abstract protected FactorySite.SiteName getSiteEnum();
    abstract protected boolean isPrivateMessage(String message);

    protected static final Spannable.Factory spannableFactory = Spannable.Factory.getInstance();
    private Handler handler;


    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }


    public void destroyLoadMessages(){
        mFuture.cancel(true);
    }

    public void prepareThread (Site site, String channel) {

        handler = new Handler();
        startThread(() -> {
            site.readChannel(channel);
        });

    }
    public  String getSiteName(){
        return  getSiteEnum().name();
    }


    abstract public void getLogin(Credentials credentials) throws LoginException;


    public HttpResponse getResponse(HttpUriRequest request) {
        HttpResponse response = null;

        try {
            HttpClient client = new DefaultHttpClient();
            response = client.execute(request);
            client.getConnectionManager().shutdown();

        } catch (IOException e) {
            Log.e(TAG, "Cant get response from site", e);
        }

        return response;
    }

    public StringBuilder jsonRequest(HttpGet httpGet) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        try {

            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
                String line;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                reader.close();
            } else {
                Log.w(TAG, "Cant get JSON. statusCode = " + statusCode);
            }

        } catch (IOException e) {
            Log.e(TAG, "Cant get JSON", e);
        }
        client.getConnectionManager().shutdown();
        return builder;
    }

    public class  PutSmile implements Runnable {
        private String smile;
        private String regexp;
        private String width;
        private String height;

        private RequestQueue queue = Volley.newRequestQueue(MyApp.getContext());

        public PutSmile(String smile, String regexp, String width, String height) {
            this.smile =smile;
            this.regexp = regexp;
            this.width = width;
            this.height = height;
        }

        public void run() {
            ImageRequest imageRequest = new ImageRequest(smile, (bmp) -> {
                    ContentValues cv = new ContentValues();
                    cv.put(MyContentProvider.SMILES_SITE, getSiteName());
                    cv.put(MyContentProvider.SMILES_SMILE, GraphicUtilities.bmpToByteArray(bmp));
                    cv.put(MyContentProvider.SMILES_REGEXP, regexp);
                    String selection = "regexp = ?";
                    String[] selectionArgs = { regexp };
                    cv.put(MyContentProvider.SMILES_WIDTH, width);
                    cv.put(MyContentProvider.SMILES_HEIGHT, height);
                    if (MyApp.getContext().getContentResolver().update(MyContentProvider.SMILE_INSERT_URI, cv, selection, selectionArgs) == 0)
                    MyApp.getContext().getContentResolver().insert(MyContentProvider.SMILE_INSERT_URI, cv);
            } ,0,0,null,null);
            queue.add(imageRequest);

        }
    }

    protected void insertMessage (Message message) {
        FactorySite.SiteName site= getSiteEnum();
        ContentValues cv = new ContentValues();
        cv.put("site", site.name());
        cv.put("channel", message.channel);
        cv.put("nick", message.nick);
        cv.put("message", message.text);
        cv.put("time", message.time);
        cv.put("identificator", site.name() + message.id);
        Cursor customCursor = MyApp.getContext().getContentResolver()
                .query(ADD_URI, new String[]{"color", "personal"}, "site = ? AND channel = ?",
                        new String[]{site.name(), message.channel}, null);
        String personal = personalSet(customCursor);
        if (personal.equalsIgnoreCase("")) personal = message.channel;
        cv.put("personal", personal);

        cv.put("color",colorSet(customCursor));
        MyApp.getContext().getContentResolver().insert(
                INSERT_URI, cv);
        customCursor.close();
        MainActivity.chatService.sendNotify(cv.getAsString("channel"), site);

    }

    protected String personalSet (Cursor c) {

        String personal = "";

        if (c.moveToNext()){personal = c.getString(1);}

        return personal;
    }

    protected int colorSet (Cursor c) {
        int color = -111111;
        if (c.moveToFirst()){color = c.getInt(0);}

        return color;
    }

    protected String getLinks(String message){
        if(Preferences.isMessageLinksShow()){
            Matcher matcher = ActionProviderLink.URL.matcher(message);
            while (matcher.find()){

                AdapterChatCursor.linkMap.add(matcher.start());
                message = message.replace(matcher.group(), "link");
                matcher = ActionProviderLink.URL.matcher(message);
            }
        }
        return message;
    }

    protected Spannable getLinkedSpan( String nick , String text) {
        int length = nick.length() + 1;
        text = getLinks(text);

        Spannable spannable = spannableFactory.newSpannable(nick + ": " + text);

        for (Integer startOfLink : AdapterChatCursor.linkMap){
            spannable.setSpan(new UnderlineSpan(), length + 1 + startOfLink ,
                    length + 1  + startOfLink + 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(MyApp.getContext().getResources()
                            .getColor(R.color.link)), length + 1 + startOfLink,
                    length + 1  + startOfLink + 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return spannable;

    }


    protected void addSmiles( Spannable spannable,  int length, String perfix) {

        ConcurrentHashMap<String, Smile> smileMap = getSmileMap();
        if (Preferences.isShowSmiles()){

            for (Map.Entry<String, Smile> entry : smileMap.entrySet()) {

                Matcher matcher = Pattern.compile(perfix + entry.getKey()).matcher(spannable);
                while (matcher.find()) {



                    for (ImageSpan span : spannable.getSpans(matcher.start(),
                            matcher.end(), ImageSpan.class))
                        if (spannable.getSpanStart(span) >= matcher.start()
                                && spannable.getSpanEnd(span) <= matcher.end()){
                            spannable.removeSpan(span);
                        }



                    spannable.setSpan(new ImageSpan(MyApp.getContext(), entry.getValue().getSmileBMP()),
                            matcher.start(), matcher.end(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                }
            }
        }

        spannable.setSpan(new ForegroundColorSpan(MyApp.getContext().getResources()
                        .getColor(R.color.nick)), 0, length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    }


    protected void sendToast(final String toast){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(MyApp.getContext(), toast, Toast.LENGTH_SHORT).show();

            }


        });
    }

    protected void privateMessage(Message message) {
        String text = message.text;
        if (Preferences.isShowNotifySystem()){
            if (isPrivateMessage(message.text)){
                text = text.replace("[b]", "").replace("[/b]", "");
                MainActivity.chatService.sendPrivateNotify(text, message.channel, getSiteEnum());
            }

        }
    }




    private ServiceConnection sConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ChatService.ChatBinder binder = (ChatService.ChatBinder) service;
            MainActivity.chatService = binder.getService();

            bound = true;

        }
        @Override
        public void onServiceDisconnected(ComponentName name) {

            bound = false;
        }
    };


}
