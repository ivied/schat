package ru.ivied.SChat.Sites.GoodGame;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.tavendo.autobahn.WebSocket;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import ru.ivied.SChat.AddChat.FragmentAddChannelStandard;
import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.Core.SendMessageService;
import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.FragmentLoginStandard;
import ru.ivied.SChat.Login.LoginException;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;
import ru.ivied.SChat.Sites.Message;
import ru.ivied.SChat.Sites.Site;
import ru.ivied.SChat.Sites.Smile;

/**
 * Created by Serv on 24.06.13.
 */
public class GoodGame extends Site {

    private static final String TAG = "GoodGameSChat";
    public static String GGNick;
    public static String GGPassWord;
    String channel;
    String channelWithoutBreakPoints;
    int channelID;
    static String GGUserID;
    ExecutorService es;
    private final String domain = "goodgame.ru";
    final String CHAT_URL = "http://www." + domain + "/chat/";
    final String LOGIN_URL = "http://" + domain + "/ajax/login/";
    @SuppressWarnings("FieldCanBeLocal")
    private final int maxServerNum = 0x1e3;
    private Random randomNum = new Random();
    private static Map<String, WebSocketForGG> ggChanelMap =  new HashMap<>();
    private static ConcurrentHashMap<String, Smile> smileMap = new ConcurrentHashMap<String, Smile>();

    private WebSocket webSocket;
    @Override
    public Drawable getLogo() {
        return MyApp.getContext().getResources().getDrawable(R.drawable.goodgame);
    }

    @Override
    public void readChannel(String channel) {
            this.channel = channel;
            this.channelWithoutBreakPoints = channel.replace(".","");
            connectToChannel();
    }


    @Override
    public void startThread(Runnable channelRun) {
        webSocket  = new WebSocketConnection();
        es = Executors.newSingleThreadExecutor();
        mFuture = es.submit(channelRun);
    }

    @Override
    public FragmentLoginStandard getFragment() {
        return new FragmentLoginGoodGame();
    }

    @Override
    public FragmentAddChannelStandard getFragmentAddChannel() {
        return new FragmentFindChannelGoodGame();
    }

    @Override
    public int getColorForAdd(String channel, int length, int color) {
        return Color.CYAN;
    }

    @Override
    public int sendMessage(String channel, String message) {
        if (GGNick == null) {
            return SendMessageService.NEED_LOGIN;
        }
        else {
            try {
                WebSocketForGG socket = ggChanelMap.get(channel);
                socket.sendMessage(message);
            } catch (RuntimeException e) {
                //cant find channel
            }
            return 0;
        }
    }

    static DefaultHttpClient httpClient;

    @Override
    public void getLogin(Credentials credentials) throws LoginException{
        GGNick = credentials.getNick();
        GGPassWord = credentials.getPass();
        httpClient = new DefaultHttpClient();
         synchronized (WebSocketForGG.waitLoginLocking){
            try {
                BasicHttpContext httpContext = new BasicHttpContext();
                CookieStore cookieStore      = httpClient.getCookieStore();
                httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
                httpClient.execute(new HttpGet("http://www." + domain), httpContext);
                setLoginCookies(cookieStore);
                HttpPost postLogin = new HttpPost(LOGIN_URL);
                setPostLoginData(postLogin);
                httpClient.execute(postLogin, httpContext);
                if (!(cookieStore.getCookies().get(8).getName().equals("uid"))) throw new LoginException();
                GGUserID = cookieStore.getCookies().get(8).getValue();
                WebSocketForGG.ready = true;
                WebSocketForGG.waitLoginLocking.notifyAll();
            } catch (IOException e) {
                Log.d(TAG, "Exception",  e);
                throw new LoginException();
            } catch (RuntimeException e){
                Log.d(TAG, "Login or password not set",  e);
                throw new LoginException();
            }
        }
    }

    @Override
    public String getSmileAddress() {
        return null;
    }

    @Override
    public void getSiteSmiles() {

    }

    @Override
    public int getMiniLogo() {
        return R.drawable.goodgame_small;
    }


    @Override
    public FactorySite.SiteName getSiteEnum() {
        return FactorySite.SiteName.GOODGAME;
    }

    @Override
    protected boolean isPrivateMessage(String message) {
        return ((GGNick != null) && (message.toLowerCase().indexOf(GGNick.toLowerCase() + ",")) == 0);
    }

    @Override
    public Spannable getSmiledText(String text, String nick) {
        int length = nick.length() + 1;
        Spannable spannable = getLinkedSpan( nick , text);
        if (isPrivateMessage(text)) {
            spannable.setSpan(new ForegroundColorSpan(MyApp.getContext().getResources()
                    .getColor(R.color.private_msg)), length, spannable.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        addSmiles( spannable, length, "");
        return spannable;
    }

    @Override
    public java.util.concurrent.ConcurrentHashMap<String, Smile> getSmileMap() {
        return smileMap;
    }

    @Override
    protected void insertMessage(Message message) {
        //bindService();
        super.insertMessage(message);
        privateMessage(message);
        //unbindService();
    }

    @Override
    public  void sendToast(String toast) {
        super.sendToast(toast);
    }

    @Override
    public void destroyLoadMessages() {
        webSocket.disconnect();
    }



    private void connectToChannel() {
        WebSocketForGG connection = new WebSocketForGG(this, webSocket);
        ggChanelMap.put(channel, connection);
        String connectUri = getConnectAddress();

        try {
            connection.connect(connectUri);

        } catch (WebSocketException e) {
            Log.d(TAG, "Exception", e);
        }

    }

    private void setPostLoginData(HttpPost httpPost) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
        nameValuePairs.clear();
        nameValuePairs.add(new BasicNameValuePair("login", GGNick));
        nameValuePairs.add(new BasicNameValuePair("password", GGPassWord));
        nameValuePairs.add(new BasicNameValuePair("remember", "1"));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "Exception",  e);
        }
        httpPost.setHeader("X-Requested-With", "XMLHttpRequest");
    }

    private void setLoginCookies(CookieStore mCookieStore) {
        BasicClientCookie cookie = new BasicClientCookie("fixed", "1");
        cookie.setDomain(domain);
        mCookieStore.addCookie(cookie) ;
        cookie = new BasicClientCookie("auto_login_name", GGNick);
        cookie.setDomain(domain);
        mCookieStore.addCookie(cookie ) ;
    }


    private String getConnectAddress() {
        return String.format("ws://%s:8080/chat/%s/%s/websocket", domain, getRandomNumberWithLeadingZeros(), getRandomString());
    }


    private String getRandomNumberWithLeadingZeros()
    {
        return String.format("%03d", randomNum.nextInt(maxServerNum));
    }

    private String getRandomString()
    {
        String[] chars = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"};
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 8; i++)  {
            builder.append(chars[random.nextInt(chars.length - 1)]);
        }
        return builder.toString();
    }

}
