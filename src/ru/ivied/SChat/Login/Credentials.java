package ru.ivied.SChat.Login;

import android.content.SharedPreferences;

import org.androidannotations.annotations.EBean;

import ru.ivied.SChat.Core.Preferences;
import ru.ivied.SChat.Sites.FactorySite;

/**
* Created by Try_harder on 04.07.2014.
*/

public class Credentials {

    private String nick;
    private String pass;

    public Credentials(String nick, String pass) {
        this.nick = nick;
        this.pass = pass;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
