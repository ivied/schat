package ru.ivied.SChat.Sites.GoodGame;

import android.graphics.drawable.Drawable;
import android.util.Log;

import ru.ivied.SChat.Login.Credentials;
import ru.ivied.SChat.Login.FragmentLoginStandard;
import ru.ivied.SChat.Login.LoginException;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;

/**
 * Created by Serv on 21.07.13.
 */
public class FragmentLoginGoodGame extends FragmentLoginStandard{
    private String TAG = "FragmentLoginGoodGameSChat";

    @Override
    public Drawable getLogo() {
        return getResources().getDrawable(R.drawable.goodgame);
    }

    @Override
    public String getFragmentName() {
        return FactorySite.SiteName.GOODGAME.name();
    }

    @Override
    public String getTextOnAddBtn() {
        return getResources().getString(R.string.btn_login_goodgame);
    }

    @Override
    public boolean tryLogin(Credentials credentials) {
        GoodGame goodGame = new GoodGame();
        try {
            goodGame.getLogin(credentials);
        } catch (LoginException e) {
            Log.d(TAG, "Negative login attempt " + e.getMessage());
            return false;
        }
        return true;
    }
}
