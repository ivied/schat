package ru.ivied.SChat.Core;

import android.app.Application;
import android.content.Context;

import org.json.JSONObject;

import ru.ivied.SChat.Sites.SmileHelper;
import ru.ivied.SChat.Utilities.MixPanel;

public class MyApp extends Application {
    private  static Context context;

    private static Preferences pref;
    static public SmileHelper SmileCache;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        SmileCache = new SmileHelper();
        MixPanel.init(getApplicationContext());
        MixPanel.track("Start App", new JSONObject());
        MixPanel.flush();

    }

    public static Context getContext() {
        return context;
    }


    public static void factoryReset() {
        // ...
    }
}

