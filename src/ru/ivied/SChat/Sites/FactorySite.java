package ru.ivied.SChat.Sites;

import ru.ivied.SChat.Sites.GoodGame.GoodGame;
import ru.ivied.SChat.Sites.Sc2tv.Sc2tv;
import ru.ivied.SChat.Sites.Twitch.Twitch;

/**
 * Created by Serv on 30.05.13.
 */
public class FactorySite {
    public enum SiteName {
        SC2TV, TWITCH, GOODGAME
    }

    public Site getSite (SiteName site) {

        switch (site){
            case SC2TV:
                return new Sc2tv();
            case TWITCH:
                return new Twitch();
            case GOODGAME:
                return new GoodGame();
        }
        return null;
    }


}
