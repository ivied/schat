package ru.ivied.SChat.Sites;

import android.graphics.Bitmap;

/**
 * Created by Try_harder on 25.05.2014.
 */
public class Smile {
    private String siteName;
    private Bitmap smileBMP;
    private String regexp;
    private Integer width;
    private Integer height;

    public Smile(String siteName, Bitmap smileBMP, String regexp, Integer width, Integer height) {
        this.siteName = siteName;
        this.smileBMP = smileBMP;
        this.regexp = regexp;
        this.width = width;
        this.height = height;
    }


    public String getSiteName() {
        return siteName;
    }

    public Bitmap getSmileBMP() {
        return smileBMP;
    }

    public String getRegexp() {
        return regexp;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }
}
