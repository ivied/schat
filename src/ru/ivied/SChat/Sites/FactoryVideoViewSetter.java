package ru.ivied.SChat.Sites;

import ru.ivied.SChat.Sites.GoodGame.GoodGameVideoSetter;
import ru.ivied.SChat.Sites.Twitch.TwitchVideoSetter;

/**
 * Created by Serv on 12.06.13.
 */
public class FactoryVideoViewSetter {


    public VideoViewSetter getVideoSite(VideoSiteName site) {

        switch (site){
            case TWITCHSTREAM:
                return new TwitchVideoSetter();
            case GOODGAMESTREAM:
                return new GoodGameVideoSetter();
        }
        return null;
    }



    public enum VideoSiteName {
        TWITCHSTREAM, GOODGAMESTREAM
    }
}
