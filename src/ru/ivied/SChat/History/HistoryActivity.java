package ru.ivied.SChat.History;


import android.app.DatePickerDialog;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.ivied.SChat.ChatView.AdapterChatCursor;
import ru.ivied.SChat.Core.MyContentProvider;
import ru.ivied.SChat.R;
import ru.ivied.SChat.Utilities.DatePickerDialogFragment;

/**
 * Created by Try_harder on 20.04.14.
 */
public class HistoryActivity extends SherlockFragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public static final String DATE_PICKER_START = "datePickerStart";
    public static final String DATE_PICKER_END = "datePickerEnd";
    Spinner spinnerForArgSelection;
    Spinner spinnerSecondSpinnerArgs;

    ListView listView;
    CheckBox checkBoxDate;
    CheckBox checkBoxRowsCount;
    EditText editTextRowCount;
    String[] amountOfVisibleRows = { "10" };
    Button filterButton;
    Loader<Cursor> loaderSort;
    boolean checkDateBox = false;

    TextView textViewStartDate;
    TextView textViewEndDate;
    DatePickerDialogFragment datePickerStart = new DatePickerDialogFragment();
    DatePickerDialogFragment datePickerFinish = new DatePickerDialogFragment();
    Calendar pickedDate = Calendar.getInstance();
    private long timeStart = pickedDate.getTimeInMillis() / 1000;
    private long timeEnd = pickedDate.getTimeInMillis() / 1000;
    long selectedTime;
    SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    AdapterChatCursor mAdapter;
    String selection = "_id > ?";
    String checkForSort = "";
    String[] argumentSelection = {"0"};
    String[] sortSelection = {"Chats", "Channels", "All"};

    final Uri SORT_SPIN_URI = Uri.parse("content://ru.ivied.SChat/channels/spin");
    final Uri CONTACT_URI = Uri.parse("content://ru.ivied.SChat/chats/history");
    final Uri ADD_URI = Uri.parse("content://ru.ivied.SChat/channels/add");

    final String TAG = "HistoryActivitySChat";

    final int CHAT_SORT = 0;
    final int CHANNEL_SORT = 1;
    final int ALL_SORT = 2;

    final int HISTORY_LOADER = 0;
    final int SORT_LOADER = 1;
    final int SPIN_LOADER = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_history);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sortSelection);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        loaderSort = getSupportLoaderManager().getLoader(SORT_LOADER);

        textViewStartDate = (TextView) findViewById(R.id.tvStartDate);
        textViewStartDate.setText("Start date " + myDateFormat.format(new Date(timeStart * 1000)));
        textViewStartDate.setOnClickListener(this);

        textViewEndDate = (TextView) findViewById(R.id.tvEndDate);
        textViewEndDate.setText("end date " + myDateFormat.format(new Date(timeStart * 1000)));
        textViewEndDate.setOnClickListener(this);

        editTextRowCount = (EditText) findViewById(R.id.etRowCount);
        editTextRowCount.setEnabled(false);

        checkBoxDate = (CheckBox) findViewById(R.id.checkBoxDate);
        checkBoxRowsCount = (CheckBox) findViewById(R.id.checkBoxRowsCount);

        checkBoxDate.setOnClickListener(this);
        checkBoxRowsCount.setOnClickListener(this);

        filterButton = (Button) findViewById(R.id.filterButton);
        filterButton.setOnClickListener(this);

        spinnerForArgSelection = (Spinner) findViewById(R.id.sortCondition);
        spinnerSecondSpinnerArgs = (Spinner) findViewById(R.id.conditionForSpinner);


        //spinner settings start
        spinnerSecondSpinnerArgs.setAdapter(adapter);
        spinnerSecondSpinnerArgs.setPrompt("Chat / channels");
        spinnerSecondSpinnerArgs.setSelection(2);
        spinnerSecondSpinnerArgs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch (position) {
                    case (CHAT_SORT):
                        checkForSort = "chat";
                        selection = "chat = ?";
                        break;
                    case (CHANNEL_SORT):
                        checkForSort = "channel";
                        selection = "channel = ?";
                        break;
                    case (ALL_SORT):
                        Spinner spinnerForArgumentSelection = (Spinner) findViewById(R.id.sortCondition);
                        spinnerForArgumentSelection.setEnabled(false);
                        checkForSort = "";
                        selection = "_id > ?";
                        String[] argumentSelectionForSpin = {"0"};
                        argumentSelection = argumentSelectionForSpin;
                        checkDateBox = false;
                        break;
                }

                if (!checkForSort.equals(""))
                    getSupportLoaderManager().initLoader(SORT_LOADER, null, HistoryActivity.this);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //spinner settings end

        String[] from = new String[]{
                MyContentProvider.MESSAGES_SITE_NAME,
                MyContentProvider.MESSAGES_NICK_NAME,
                MyContentProvider.MESSAGES_MESSAGE,
                MyContentProvider.MESSAGES_PERSONAL,
                MyContentProvider.MESSAGES_COLOR,
        };

        int[] to = new int[]{R.id.ivImg, 0, R.id.tvText, R.id.channelName, 0};

        mAdapter = new AdapterChatCursor(this, R.layout.message,
                null, from, to, 0);

        listView = (ListView) findViewById(R.id.history);
        listView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(HISTORY_LOADER, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {

        Loader<Cursor> mLoader;

        switch (id) {
            case (HISTORY_LOADER):
                mLoader = new CursorLoader(this,
                        CONTACT_URI, amountOfVisibleRows, selection, argumentSelection, null);
                return mLoader;

            case (SORT_LOADER):
                mLoader = new CursorLoader(this,
                        SORT_SPIN_URI, null, null, null, null);
                return mLoader;

            case (SPIN_LOADER):
                mLoader = new CursorLoader(this,
                        CONTACT_URI, amountOfVisibleRows, selection, argumentSelection, null);
                return mLoader;

        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case (HISTORY_LOADER):
                Log.i(TAG, "load finished");
                mAdapter.swapCursor(data);
                break;
            case (SPIN_LOADER):
                loaderSort = loader;
                Log.i(TAG, "load finished");
                mAdapter.swapCursor(data);
                mAdapter.notifyDataSetChanged();
                break;
            case (SORT_LOADER):
                String item;
                List<String> list = new ArrayList<String>();
                if (checkForSort.equals("chat")) {
                    for (data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
                        item = data.getString(data.getColumnIndex("chat"));
                        if (!list.contains(item))
                            list.add(item);
                    }
                }
                if (checkForSort.equals("channel")) {
                    for (data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
                        item = data.getString(data.getColumnIndex("channel"));
                        if (!list.contains(item))
                            list.add(item);
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinnerForArgSelection.setEnabled(true);
                spinnerForArgSelection.setAdapter(adapter);
                spinnerForArgSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        String[] argumentSelectionForSpin = {parent.getItemAtPosition(position).toString()};
                        if (checkForSort.equals("chat")) {
                            String[] name = argumentSelectionForSpin;
                            Cursor c = HistoryActivity.this.getContentResolver().query(ADD_URI, null,
                                    "chat = ?", name, null);
                            String[] selectionArgs = new String[c.getCount()];
                            List<String> channels = new ArrayList<String>();
                            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                                // The Cursor is now set to the right position

                                channels.add(c.getString(c.getColumnIndex("channel")));

                            }
                            c.close();
                            selectionArgs = channels.toArray(selectionArgs);
                            selection = "";
                            for (int i = 0; i < channels.size(); i++) {
                                if (i > 0)
                                    selection = selection.concat("OR ");
                                selection = selection.concat("channel = ? ");

                            }
                            argumentSelectionForSpin = selectionArgs;
                        }
                        argumentSelection = argumentSelectionForSpin;
                        checkDateBox = false;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View view) {

        int ArgIndex;

        switch (view.getId()) {
            case R.id.checkBoxDate:
                if (!checkBoxDate.isChecked()) {
                    if (checkDateBox) {
                        selection = selection.substring(0, selection.indexOf(" AND time > ? AND time < ? "));
                        String[] argS = new String[argumentSelection.length - 2];
                        List<String> channels = new ArrayList<String>();
                        for (ArgIndex = 0; ArgIndex < (argumentSelection.length - 2); ArgIndex++)
                            channels.add(ArgIndex, argumentSelection[ArgIndex]);
                        argumentSelection = channels.toArray(argS);
                        checkDateBox = false;

                    }
                }
                break;
            case R.id.tvStartDate:
                if (checkBoxDate.isChecked()) {
                    showDatePickerDialog(view);
                }
                break;
            case R.id.tvEndDate:
                if (checkBoxDate.isChecked()) {
                    showDatePickerDialog(view);
                }
                break;
            case R.id.checkBoxRowsCount:
                editTextRowCount.setEnabled(checkBoxRowsCount.isChecked());
                break;
            case R.id.filterButton:
                if (checkBoxRowsCount.isChecked() && (!editTextRowCount.getText().toString().equals("")))
                    amountOfVisibleRows[0] = editTextRowCount.getText().toString();
                if (checkBoxDate.isChecked()) {
                    int argSelectionLength;
                    if (!selection.contains("time"))
                        selection = selection + " AND time > ? AND time < ? ";
                    if (!checkDateBox) {
                        argSelectionLength = argumentSelection.length + 2;
                    } else {
                        argSelectionLength = argumentSelection.length;
                    }

                    String[] argS = new String[argSelectionLength];
                    List<String> channels = new ArrayList<String>();
                    if (!checkDateBox) {
                        for (ArgIndex = 0; ArgIndex < (argSelectionLength - 2); ArgIndex++)
                            channels.add(ArgIndex, argumentSelection[ArgIndex]);
                        checkDateBox = true;
                        channels.add(ArgIndex, String.valueOf(timeStart));
                        channels.add(ArgIndex + 1, String.valueOf(timeEnd));
                    } else {
                        for (ArgIndex = 0; ArgIndex < (argSelectionLength); ArgIndex++)
                            channels.add(ArgIndex, argumentSelection[ArgIndex]);
                        channels.set(ArgIndex - 2, String.valueOf(timeStart));
                        channels.set(ArgIndex - 1, String.valueOf(timeEnd));
                    }


                    argS = channels.toArray(argS);
                    argumentSelection = argS;
                }
                if (loaderSort != null && (!loaderSort.isReset())) {
                    getSupportLoaderManager().restartLoader(SPIN_LOADER, null, HistoryActivity.this);
                } else {
                    getSupportLoaderManager().initLoader(SPIN_LOADER, null, HistoryActivity.this);
                }
                break;

        }
    }


    public void showDatePickerDialog(View v) {

        Bundle b = new Bundle();
        Calendar c = Calendar.getInstance();

        b.putInt(DatePickerDialogFragment.YEAR, c.get(Calendar.YEAR));
        b.putInt(DatePickerDialogFragment.MONTH, c.get(Calendar.MONTH));
        b.putInt(DatePickerDialogFragment.DATE, c.get(Calendar.DAY_OF_MONTH));

        switch (v.getId()) {
            case R.id.tvStartDate:
                datePickerStart.setArguments(b);
                datePickerStart.show(getSupportFragmentManager(), DATE_PICKER_START);
                break;
            case R.id.tvEndDate:
                datePickerFinish.setArguments(b);
                datePickerFinish.show(getSupportFragmentManager(), DATE_PICKER_END);
                break;
        }

    }


    public void onDateSet(DatePicker view, int year, int month, int day) {

        pickedDate.set(Calendar.YEAR, year);
        pickedDate.set(Calendar.MONTH, month);
        pickedDate.set(Calendar.DAY_OF_MONTH, day);
        selectedTime = pickedDate.getTimeInMillis() / 1000;

        if (getSupportFragmentManager().getFragments().get(0).getTag().equalsIgnoreCase(DATE_PICKER_START)) {
            timeStart = selectedTime;
            textViewStartDate.setText("Start date " + myDateFormat.format(new Date(timeStart * 1000)));
        } else if (getSupportFragmentManager().getFragments().get(0).getTag().equalsIgnoreCase(DATE_PICKER_END)) {
            timeEnd = selectedTime;
            textViewEndDate.setText("end date " + myDateFormat.format(new Date(timeEnd * 1000)));
        }
    }
}


