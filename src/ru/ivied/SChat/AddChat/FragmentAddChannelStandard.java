package ru.ivied.SChat.AddChat;

import android.widget.EditText;

import com.actionbarsherlock.app.SherlockFragment;

/**
 * Created by Serv on 03.06.13.
 */
abstract public class FragmentAddChannelStandard extends SherlockFragment{
    abstract public EditText getEditTextChannel();
}
