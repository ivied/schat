package ru.ivied.SChat.Sites;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;

import ru.ivied.SChat.Core.MyApp;
import ru.ivied.SChat.Core.Preferences;

/**
 * Created by Serv on 03.06.13.
 */
public class SmileHelper  {

    private static boolean isRefresh = false;


    public  SmileHelper() {
        for (FactorySite.SiteName siteName : FactorySite.SiteName.values()){
            SmileParser smileParser = new SmileParser();
            smileParser.execute(siteName);
            if (!isRefresh) {
                SmileHandler_.getInstance_(MyApp.getContext());
            }

        }
    }


    static class SmileParser extends AsyncTask<FactorySite.SiteName, Boolean, Void> {
        FactorySite factorySite = new FactorySite();


        Site site;
        @Override
        protected Void doInBackground(FactorySite.SiteName... params) {

            if (Preferences.isUpdateSmiles() || isRefresh){
            site = factorySite.getSite(params[0]);
             if(site.getSmileAddress() == null) return null;

            HttpGet httpGet = new HttpGet(site.getSmileAddress());
            HttpResponse response = site.getResponse(httpGet);
            if (response == null) return null;

            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (!(statusCode == 200)) return null;

            Thread myThread = new Thread(site::getSiteSmiles);

            myThread.start();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Boolean... newSmiles) {
            super.onProgressUpdate(newSmiles);
//            if (newSmiles[0]) site.setSmileMaps();
        }



        @Override
        protected void onPostExecute(Void result) {


            super.onPostExecute(result);

        }
    }


    public void Refresh() {
        isRefresh = true;
       new SmileHelper();
    }
}
