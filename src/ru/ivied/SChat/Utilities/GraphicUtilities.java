package ru.ivied.SChat.Utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;

/**
 * Created by Try_harder on 24.05.2014.
 */
public class GraphicUtilities {

    public static byte[] bmpToByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static Bitmap getImageFromBLOB(byte[] mBlob, float smileSizeMultiplicator) {

        return getResizedBitmap(BitmapFactory.decodeByteArray(mBlob, 0, mBlob.length), smileSizeMultiplicator);

    }

    public static Bitmap getResizedBitmap(Bitmap bm, float smileSizeMultiplicator) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(smileSizeMultiplicator, smileSizeMultiplicator);

        // "RECREATE" THE NEW BITMAP
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }
}
