package ru.ivied.SChat.Login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockDialogFragment;

import java.util.ArrayList;

import ru.ivied.SChat.R;
import ru.ivied.SChat.Sites.FactorySite;

/**
* Created by Serv on 06.07.2014.
*/
public class DialogSelectLogin extends SherlockDialogFragment {
    ArrayList<String> siteNames;
    public Dialog onCreateDialog(Bundle savedInstanceState) {
         siteNames =new  ArrayList < String >();
        for( FactorySite.SiteName siteName : FactorySite.SiteName.values()){
            siteNames.add(siteName.name());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.select_dialog_singlechoice, siteNames);
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(R.string.dialog_select_login_title);
        adb.setSingleChoiceItems(adapter, -1, myClickListener);
        adb.setPositiveButton(R.string.dialog_select, myClickListener);
        return adb.create();


    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {


            ListView lv = ((AlertDialog) dialog).getListView();
            if (which == Dialog.BUTTON_POSITIVE) {
                if (lv.getCheckedItemPosition()>=0){
                    deletePass(siteNames.get(lv.getCheckedItemPosition()));

                }
            }
        }
    };

    private void deletePass(String site) {

        deletePassword(site);
        Toast.makeText(getActivity(), site + " " + getResources().getString(R.string.Login_delete), Toast.LENGTH_SHORT).show();
    }

    private  void deletePassword(String site) {
        SharedPreferences sPref = getActivity().getSharedPreferences(Login.XML_LOGIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.remove(site);
        ed.remove(site + Login.PASSWORD);
        ed.commit();
    }
}
